﻿/// <summary>
/// Simple Singleton iplementation
/// </summary>
namespace AdsyTools
{
    /// <summary>
    /// Basic Singleton helper
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Singleton<T> where T : new()
    {
        private static T instance;

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new T();
                }
                return instance;
            }
        }
    }
}
