﻿
namespace AdsyTools
{
    public class DispatcherUpdater : UnitySingleton<DispatcherUpdater>
    {
        private void Update()
        {
            Dispatcher.Instance.InvokePending();
        }
    }
}
