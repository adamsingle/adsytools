﻿
using MongoDB.Driver;
using System;
using System.Linq;
using System.Collections.Generic;

namespace AdsyTools
{
    public class MongoDBManager : Singleton<MongoDBManager>
    {
        private class Connection
        {
            public string url;

            public MongoServer server;
        }

        Dictionary<Guid, Connection> connections = new Dictionary<Guid, Connection>();

        public Guid Connect(string url)
        {
            //if the connection already exists, return the server
            var connection = connections.Where(conn => string.Compare(conn.Value.url, url, true) == 0).FirstOrDefault();

            if (!connection.Equals(default(KeyValuePair<Guid, Connection>)))
            {
                return connection.Key;
            }

            var client = new MongoClient(url);

            var server = client.GetServer();

            var guid = Guid.NewGuid();

            connections.Add(guid, new Connection() { server = server, url = url });

            return guid;
        }

        /// <summary>
        /// Attempt to get a named collection from a named database attached to the server Guid provided.
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="db"></param>
        /// <param name="collection"></param>
        /// <returns></returns>
        public MongoCollection GetCollection(Guid guid, string db, string collection)
        {
            if (!connections.ContainsKey(guid))
            {
                throw new ApplicationException("connection guid doesn't match any on record. Use Connect(url) to establish a connection and get a guid for it");
            }

            MongoDatabase database;

            try
            {
                database = connections[guid].server.GetDatabase(db);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Failed to get database {0} from the connected server. Error: {1}.", db, ex.Message));
            }

            try
            {
                var col = database.GetCollection(collection);
                return col;
            }
            catch(Exception ex)
            {
                throw new ApplicationException(string.Format("Failed to get collection {0} from the database {1}. Error: {2}", collection, db, ex.Message));
            }
        }

        /// <summary>
        /// Attempt to get a named collection from a named database attached to the server Guid provided.
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="db"></param>
        /// <param name="collection"></param>
        /// <returns></returns>
        public MongoCollection GetCollection<TEntity>(Guid guid, string db, string collection)
        {
            if (!connections.ContainsKey(guid))
            {
                throw new ApplicationException("connection guid doesn't match any on record. Use Connect(url) to establish a connection and get a guid for it");
            }

            MongoDatabase database;

            try
            {
                database = connections[guid].server.GetDatabase(db);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Failed to get database {0} from the connected server. Error: {1}.", db, ex.Message));
            }

            try
            {
                var col = database.GetCollection<TEntity>(collection);
                return col;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("Failed to get collection {0} from the database {1}. Error: {2}", collection, db, ex.Message));
            }
        }
    }
}
