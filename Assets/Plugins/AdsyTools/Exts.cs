﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace AdsyTools
{
    /// <summary>
    /// Collection of helpful extension methods
    /// </summary>
    public static class Exts
    {
        /// <summary>
        /// A foreach that can be used in a LINQ expression
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="action"></param>
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (T item in source)
            {
                action(item);
            }
        }

        /// <summary>
        /// Convert a 3 or 4 float array (RGB or RGBA) into a UnityEngine.Color
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static Color RGBToColor(this float[] source)
        {
            if (source.Length < 3 || source.Length > 4)
            {
                return Color.white;
            }

            var colour = new Color();
            colour.r = source[0];
            colour.g = source[1];
            colour.b = source[2];
            if (source.Length == 4)
            {
                colour.a = source[3];
            }
            return colour;
        }

        /// <summary>
        /// Breadth first recursive search for a named child
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Transform BreadthFirstFindChild(this Transform parent, string name)
        {
            var result = parent.Find(name);
            if (result != null)
            {
                return result;
            }

            foreach (Transform child in parent)
            {
                result = child.BreadthFirstFindChild(name);
                if (result != null)
                {
                    return result;
                }
            }
            return null;
        }

        /// <summary>
        /// Depth first recursive search for a names child
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Transform DepthFirstFindChild(this Transform parent, string name)
        {
            foreach (Transform child in parent)
            {
                if (child.name == name)
                {
                    return child;
                }
                var result = child.DepthFirstFindChild(name);
                if (result != null)
                {
                    return result;
                }
            }
            return null;
        }

        /// <summary>
        /// Extension for generating a random value on a unit circle
        /// </summary>
        /// <returns></returns>
        public static Vector2 OnUnitCircle()
        {
            float randomAngle = UnityEngine.Random.Range(0f, Mathf.PI * 2);
            return new Vector2(Mathf.Sin(randomAngle), Mathf.Cos(randomAngle)).normalized;
        }

        /// <summary>
        /// TryAdd a keyvalue pair to a dictionary
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="source"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool TryAdd<TKey, TValue>(this Dictionary<TKey, TValue> source, TKey key, TValue value)
        {
            if (source.ContainsKey(key))
            {
                return false;
            }
            source.Add(key, value);
            return true;
        }

        /// <summary>
        /// Draw random entries from a dictionary
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="source"></param>
        /// <param name="numberOfValues"></param>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<TKey, TValue>> GetRandomUniqueValues<TKey, TValue>(this IDictionary<TKey, TValue> source, int numberOfValues)
        {
            System.Random rand = new System.Random();

            //Ensure the number of values isn't more than the number of entries in the dictionary
            numberOfValues = Math.Min(numberOfValues, source.Count);

            LinkedList<TKey> keys = new LinkedList<TKey>(source
                                                                .Select(keyvalue => keyvalue.Key)
                                                                .OrderBy(x => rand.Next()));
            while (numberOfValues > 0)
            {
                --numberOfValues;
                yield return new KeyValuePair<TKey, TValue>(keys.Last.Value, source[keys.Last.Value]);
                keys.RemoveLast();
            }

        }
    }
}
