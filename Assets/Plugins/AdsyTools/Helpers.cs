﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG;
using UnityEditorInternal;

namespace AdsyTools
{
    /// <summary>
    /// A collection of static helper functions
    /// </summary>
    public static class Helpers
    {
        /// <summary>
        /// A helper function to merge multiple dictionaries that have integer values together. Any matching keys between dictionaries will have their values summed in the 
        /// final result dictionary. 
        /// Use case example: Collating round scores of user ids to find final scores
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="dictionaries"></param>
        /// <returns></returns>
        public static Dictionary<TKey, int> MergeDictionariesWithIntValues<TKey>(IEnumerable<Dictionary<TKey, int>> dictionaries)
        {
            var result = dictionaries.SelectMany(dict => dict)
                .ToLookup(pair => pair.Key, pair => pair.Value)
                .ToDictionary(group => group.Key, group => group.Sum(x => x));

            return result;
        }

        /// <summary>
        /// Providing a collection of Func<IPromise> and a max concurrency, this method will batch them based on the 
        /// supplied maxConcurrency. Each promise in a batch is run in parallel, while the batches are sequenced. Think of it
        /// as a throttled Promise.All. The returned promise is resolved once all the promises have resolved.
        /// </summary>
        /// <param name="promises"></param>
        /// <param name="maxConcurrency"></param>
        /// <returns></returns>
        public static IPromise ConcurrentPromiseQueue(IEnumerable<Func<IPromise>> promises, int maxConcurrency)
        {
            var loopCount = 0;
            var promisesArray = promises as Func<IPromise>[] ?? promises.ToArray();
            var refCount = promisesArray.Count();
            List<Func<IPromise>> bundledPromisesToSequence = new List<Func<IPromise>>();

            while (refCount > 0)
            {
                var pendingPromises = promisesArray.Count() >= maxConcurrency
                    ? promisesArray.Skip(loopCount * maxConcurrency).Take(maxConcurrency)
                    : promisesArray.Skip(loopCount * maxConcurrency).Take(promisesArray.Count() - refCount);

                bundledPromisesToSequence.Add(() => Promise.All(pendingPromises.Select(p => p())));
                loopCount++;
                refCount -= maxConcurrency;
            }

            return Promise.Sequence(bundledPromisesToSequence);
        }
    }
}
