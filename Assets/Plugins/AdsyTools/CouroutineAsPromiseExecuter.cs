﻿
using RSG;
using System;
using System.Linq;
using System.Collections;

namespace AdsyTools
{
    /// <summary>
    /// The MonoBehaviour that runs coroutines as promises. 
    /// This is a Unity Singelton
    /// </summary>
    public class CoroutineAsPromiseExecuter : UnitySingleton<CoroutineAsPromiseExecuter>
    {
        public IPromise ExecuteSequence(Func<IEnumerator>[] coroutines)
        {
            Func<IPromise>[] promises = coroutines.Select(coroutine =>
            {
                Func<IPromise> prom = () => Execute(coroutine);
                return prom;
            }).ToArray();

            return Promise.Sequence(promises);
        }

        public IPromise Execute(Func<IEnumerator> coroutine)
        {
            return new Promise((resolve, reject) =>
                StartCoroutine(RunTheCoroutine(coroutine, resolve, reject))
            );
        }

        private IEnumerator RunTheCoroutine(Func<IEnumerator> coroutineFunc, Action resolve, Action<Exception> reject)
        {
            var routine = StartExceptionalCoroutine(coroutineFunc());
            yield return routine.coroutine;

            if (routine.e == null)
            {
                resolve();
            }
            else
            {
                reject(routine.e);
            }
        }

        /// <summary>
        /// Wrap the coroutine in an ExceptionCoroutine
        /// </summary>
        /// <param name="coroutine"></param>
        /// <returns></returns>
        private ExceptionalCoroutine StartExceptionalCoroutine(IEnumerator coroutine)
        {
            ExceptionalCoroutine coroutineObj = new ExceptionalCoroutine();
            coroutineObj.coroutine = StartCoroutine(coroutineObj.InternalRoutine(coroutine));
            return coroutineObj;
        }
    }
}
