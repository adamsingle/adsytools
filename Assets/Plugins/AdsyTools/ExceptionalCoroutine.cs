﻿using System;
using System.Collections;
using UnityEngine;

namespace AdsyTools
{
    /// <summary>
    /// A wrapper for a coroutine that will handle exceptions
    /// </summary>
    public class ExceptionalCoroutine
    {
        public Exception e = null;

        public Coroutine coroutine;

        public IEnumerator InternalRoutine(IEnumerator coroutine)
        {
            while (true)
            {
                try
                {
                    if (!coroutine.MoveNext())
                    {
                        yield break;
                    }
                }
                catch (Exception e)
                {
                    this.e = e;
                    yield break;
                }

                yield return coroutine.Current;
            }
        }
    }
}
