﻿
using RSG;
using System;
using System.Collections;
using UnityEngine;

namespace AdsyTools
{
    /// <summary>
    /// A wrapper
    /// </summary>
    public class AdsyToolsPromiseTimer : PromiseTimer
    {
        /// <summary>
        /// Repeat a promise based function until a given predicate resolves to true, at which point the promise returned by RepeatUntil will resolve.
        /// </summary>
        /// <param name="promiseToRun"></param>
        /// <param name="predicate"></param>
        /// <returns>Promise that resolves once the predicate resolves to true</returns>
        public IPromise RepeatUntil(Func<IPromise> promiseToRun, Func<bool> predicate)
        {
            var promise = new Promise();

            RepeatPromise(promiseToRun, promise, predicate);

            return promise;
        }

        /// <summary>
        /// Wrapper for the WaitForEndOfFrame enumerator provided by Unity. 
        /// </summary>
        /// <returns>Promise</returns>
        public IPromise WaitForEndOfFrame()
        {
            return CoroutineAsPromiseExecuter.Instance.Execute(() => WaitForEndOFFrameCoroutine());
        }

        private IEnumerator WaitForEndOFFrameCoroutine()
        {
            yield return new WaitForEndOfFrame();
        }

        private IPromise RepeatPromise(Func<IPromise> promiseToRun, Promise pendingPromise, Func<bool> predicate)
        {
            promiseToRun()
                .Then(() =>
                {
                    if (predicate())
                    {
                        pendingPromise.Resolve();
                    }
                    else
                    {
                        RepeatPromise(promiseToRun, pendingPromise, predicate);
                    }
                });
            return pendingPromise;
        }
    }
}
