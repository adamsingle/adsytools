﻿using UnityEngine;

namespace AdsyTools
{
    /// <summary>
    /// An implementation of a Singleton for monobehaviours. The scene will be checked first, and if not found, a game object will be created with the component
    /// attached.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class UnitySingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        protected static T instance;

        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (T)FindObjectOfType(typeof(T));

                    if (instance == null)
                    {
                        var go = new GameObject();
                        instance = go.AddComponent<T>();
                    }
                }
                return instance;
            }
        }
    }
}
