﻿using System;
using System.Collections.Generic;

namespace AdsyTools
{
    public class Dispatcher : Singleton<Dispatcher>
    {
        private Queue<Action> pendingActions = new Queue<Action>();

        public void Invoke(Action action)
        {
            lock (pendingActions)
            {
                pendingActions.Enqueue(action);
            }
        }

        public void InvokePending()
        {
            lock (pendingActions)
            {
                while (pendingActions.Count > 0)
                {
                    pendingActions.Dequeue()();
                }
            }
        }
    }
}
